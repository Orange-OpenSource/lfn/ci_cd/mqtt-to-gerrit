# MQTT to Gerrit

Goal of this app is to listen to MQTT topics and send them to the right gerrit
review.

Each reviews will be posted in a the `master_topic` with a message being a
`JSON` object, with the following mandatory fields:

```JSON
{
    "gerrit_review": "1234",
    "gerrit_patchset": "3",
    "score": "0",
    "from": "the_sender",
    "message": "the message"
}
```

In this example, this will create a comment with message
`the_sender: the message` on review`1234`, patchset `3` with score `0`.

## Configuration

configuration is done via a configuration file
an example is in etc/gerrit-to-mqtt.conf:

```gitconfig
[mqtt]
hostname=localhost
topic=gerrit/myproject
# Only if needed for websockets
ws_path=/path
# transport is "websockets" or "tcp"
# default is "tcp"
transport=websockets
# If qos is not set, default to 0
qos=2
# If port is not set, default to 1883
port=1883
# If you need authentication on the MQTT Broker
username=username
password=password

[gerrit]
username=gerrit_username
hostname=localhost
key=~/.ssh/id_rsa
# if port not set, default to 29418
port=29418
# set proxy only if needed
# socks proxy only is supported today
proxy_type=socks
proxy_host=host
proxy_port=port
```

## Usage

### Command Line

After installation of the requirements, launch the script with the path of the
configuration file as arguments

```bash
pip install -r requirements.txt
python mqtt-to-gerrit.py etc/mqtt-to-gerrit.conf
```

### Docker

you'll have to put the right path of the id_rsa key in `mqtt-to-gerrit.conf`. In
the example below, it would be `key=/etc/ssh/id_rsa`.

```bash
docker run  --volume path/to/configuration:/etc/mqtt-to-gerrit.conf:ro \
            --volume path/to/id_rsa:/etc/ssh/id_rsa:ro \
    registry.gitlab.com/orange-opensource/lfn/ci_cd/mqtt-to-gerrit:latest
```

## TODO

- [ ] put log directly on output
- [ ] choose log level in the configuration and via the command line
