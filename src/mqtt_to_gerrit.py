#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""
MQTT to Gerrit.

Listen to a MQTT topic and push message to the right review from a Gerrit.
"""
import argparse
import json
import logging
import logging.config

from configparser import RawConfigParser
from requests import HTTPError, RequestException

import paho.mqtt.client as mqtt
from pygerrit2 import GerritReview, GerritRestAPI, HTTPBasicAuth

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)-14s - %(levelname)-8s - %(message)s')
logging.captureWarnings(True)
LOGGER = logging.getLogger("mqtt_to_gerrit")


def on_connect(client, userdata, _flags, return_code):
    """
    Use when MQTT client is connected.

    Function launched when MQQT client is connected.
    After connection, subscribe to the review topic is done.

    :param client: the client that has been connected
    :param _userdata: the userdata defined on the client side
    :param _flags: the flags received
    :param return_code: return code of the connection

    :type client: paho.mqtt.client.Client
    :type userdata: a dict with at least the following entries:
        - topic: the root topic where we listen all the messages
        - master_topic: the topic used by workers to talk to the master
        - worker_topic: the topic used by master to talk to the workers
        - qos: the QoS for the messenging part
    :type flags: string -- not used --
    :type return_code: integer

    :seealso: http://www.eclipse.org/paho/clients/python/docs/#callbacks
    """
    LOGGER.info("connected to MQTT Broker with status %s", str(return_code))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    LOGGER.info("subscribing on topic %s with qos %s",
                userdata['topic'], str(userdata['qos']))
    client.subscribe(("{}/#".format(userdata['topic']), userdata['qos']))

def on_message(_client, userdata, msg):
    """
    Use when message is received.

    Function launched when a message is received on MQTT topic.
    Once retrieved the message, we post the message to the right Gerrit review.

    :param _client: the client that has received the message
    :param userdata: the userdata defined on the client side
    :param msg: the message object
    :type _client: paho.mqtt.client.Client  -- not used --
    :type userdata: a dict with at least the following entries:
        - gerrit: the Gerrit informations as a :class:`GerritRestAPI`
    :type msg: paho.mqtt.client.MQTTMessage

    :seealso: http://www.eclipse.org/paho/clients/python/docs/#callbacks
    """
    LOGGER.debug("received a message")
    LOGGER.debug("message: %s", msg.payload)
    gerrit = userdata['gerrit']

    try:
        LOGGER.debug("parsing the message")
        message = json.loads(msg.payload.decode('UTF-8'))
        LOGGER.debug("retrieving values")
        review_id = message["gerrit_review"]
        patchset = message["gerrit_patchset"]
        score = message["score"]
        commit_message = "{}: {}".format(message["from"], message["message"])
        LOGGER.debug("Sending message '%s' on review %s, patchset %s with \
                     score %s", commit_message, review_id, patchset, score)

        review = GerritReview()
        review.set_message(commit_message)
        review.add_labels({"Code-Review": score})
        response = gerrit.review(review_id, patchset, review)
        LOGGER.info('review sent: %s', response)
    except (ValueError, HTTPError, RequestException) as exc:
        LOGGER.exception(exc)
        LOGGER.debug("review not sent")

def _get_options():
    """
    Define and parse the command line arguments.

    :returns: Namespace of the configuration
    :seealso: https://docs.python.org/3/library/argparse.html#the-parse-args-method
    """
    description = "Listen to a MQTT Topic and send back the informations to a \
                   Gerrit."
    parser = argparse.ArgumentParser(
        description=description)
    parser.add_argument("-v", "--verbose", help="Increase output verbosity",
                        action="store_const", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('conffile', nargs=1, help="Configuration file")
    return parser.parse_args()

def _get_config(config_file):
    """
    Read the config file and return the configuration.

    :param config_file: the config file name
    :type config_file: string
    :returns: configparser.ConfigParser object
    :seealso: https://docs.python.org/3/library/configparser.html
    """
    LOGGER.info("retrieving %s configuration file", config_file)
    config = RawConfigParser()
    config.read(config_file)
    return config

def _parse_config(config):
    """
    Parse needed configuration.

    :params config: the whole configuration
    :type config: configparser.ConfigParser object
    :returns: config
    :rtype: dict
    """
    LOGGER.info("parsing configuration")
    auth = None
    username = config.get('mqtt', 'username', fallback=None)
    password = config.get('mqtt', 'password', fallback=None)
    if username:
        auth = {'username': username}
        if password:
            auth['password'] = password

    return {
        'mqtt': {
            'qos': config.getint('mqtt', 'qos', fallback=0),
            'port': config.getint('mqtt', 'port', fallback=1883),
            'keepalive': config.getint('mqtt', 'keepalive', fallback=60),
            'transport': config.get('mqtt', 'transport', fallback="tcp"),
            'hostname': config.get('mqtt', 'hostname'),
            'auth': auth
        },
        'mqtt_path': config.get('mqtt', 'ws_path', fallback=None),
        'mqtt_headers': config.get('mqtt', 'ws_headers', fallback=None),
        'topics': {
            'main': config.get('mqtt', 'topic'),
        },
        'gerrit':{
            'hostname': config.get('gerrit', 'hostname'),
            'username': config.get('gerrit', 'username'),
            'password': config.get('gerrit', 'password')
        }
    }

def main():
    """
    Enter into the main function.

    Do the following actions, according to the config file:
        - subscribe to the right MQTT topics
        -
    """
    args = _get_options()
    config = _parse_config(_get_config(args.conffile))

    # Configure Gerrit
    LOGGER.debug("Gerrit client creation")
    LOGGER.info("using gerrit host %s with user %s",
                config['gerrit']['hostname'],
                config['gerrit']['username'])
    gerrit_auth = HTTPBasicAuth(config['gerrit']['username'],
                                config['gerrit']['password'])
    gerrit = GerritRestAPI(url=config['gerrit']['hostname'], auth=gerrit_auth)

    LOGGER.debug("MQTT client creation")
    client = mqtt.Client(transport=config['mqtt']['transport'])
    client.user_data_set({'topic': config['topics']['main'],
                          'qos': config['mqtt']['qos'],
                          'gerrit': gerrit})
    client.enable_logger(logging.getLogger("mqtt_client"))
    client.on_connect = on_connect
    client.on_message = on_message
    if config['mqtt']['transport'] == "websockets":
        client.ws_set_options(path=config['mqtt_path'],
                              headers=config['mqtt_headers'])
    LOGGER.debug("MQTT client connection attempt")
    client.connect(config['mqtt']['hostname'], config['mqtt']['port'],
                   config['mqtt']['keepalive'])
    client.loop_forever()

if __name__ == "__main__":
    main()
